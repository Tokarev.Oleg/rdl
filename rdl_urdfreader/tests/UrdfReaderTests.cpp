//
// Created by jordan on 5/18/17.
//

#include <fstream>
#include <gtest/gtest.h>
#include <ros/package.h>

#include "rdl_dynamics/Dynamics.h"
#include "rdl_dynamics/Kinematics.h"
#include "rdl_dynamics/Model.h"
#include "rdl_urdfreader/urdfreader.h"

class UrdfReaderTests : public testing::Test
{
  public:
    UrdfReaderTests()
    {
        srand(time(nullptr));
    }

    void SetUp()
    {
    }

    void TearDown()
    {
    }

    bool checkSpatialMatrixEpsilonClose(const RobotDynamics::Math::SpatialMatrix& t1, const RobotDynamics::Math::SpatialMatrix& t2, const double epsilon)
    {
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                if (fabs(t1(i, j) - t2(i, j)) > epsilon)
                {
                    return false;
                }
            }
        }

        return true;
    }

    bool checkSpatialVectorsEpsilonClose(const RobotDynamics::Math::SpatialVector& v1, const RobotDynamics::Math::SpatialVector& v2, const double epsilon)
    {
        for (int i = 0; i < 6; i++)
        {
            if (fabs(v1(i) - v2(i)) > epsilon)
            {
                return false;
            }
        }

        return true;
    }

    std::string getFileContents(const std::string& filename)
    {
        std::ifstream model_file(filename);
        if (!model_file)
        {
            std::cerr << "Error opening file '" << filename << "'." << std::endl;
            abort();
        }

        // reserve memory for the contents of the file
        std::string model_xml_string;
        model_file.seekg(0, std::ios::end);
        model_xml_string.reserve(model_file.tellg());
        model_file.seekg(0, std::ios::beg);
        model_xml_string.assign((std::istreambuf_iterator<char>(model_file)), std::istreambuf_iterator<char>());

        model_file.close();
        return model_xml_string;
    }
};

TEST_F(UrdfReaderTests, q_index_two_chains)
{
    RobotDynamics::ModelPtr model(new RobotDynamics::Model());
    std::string path_to_urdf = ros::package::getPath("rdl_urdfreader") + "/tests/urdf/simple_two_chain_bot.urdf";

    std::string contents = getFileContents(path_to_urdf);
    std::vector<unsigned int> q_indices;
    std::vector<std::string> joint_names, body_names;
    EXPECT_TRUE(RobotDynamics::Urdf::parseJointAndBodyNamesFromString(contents, joint_names, body_names));
    EXPECT_TRUE(RobotDynamics::Urdf::parseJointAndQIndex(contents, q_indices));

    EXPECT_EQ(q_indices.size(), 3);
    EXPECT_STREQ(joint_names[0].c_str(), "test_robot_shoulder_lift_joint");
    EXPECT_STREQ(body_names[0].c_str(), "test_robot_upper_arm_link");
    EXPECT_EQ(q_indices[std::distance(joint_names.begin(), std::find(joint_names.begin(), joint_names.end(), "test_robot_shoulder_lift_joint"))], 0);

    EXPECT_STREQ(joint_names[1].c_str(), "test_robot_elbow_joint");
    EXPECT_STREQ(body_names[1].c_str(), "test_robot_forearm_link");
    EXPECT_EQ(q_indices[std::distance(joint_names.begin(), std::find(joint_names.begin(), joint_names.end(), "test_robot_elbow_joint"))], 1);

    EXPECT_STREQ(joint_names[2].c_str(), "test_robot_shoulder_pan_joint");
    EXPECT_STREQ(body_names[2].c_str(), "test_robot_shoulder_link");
    EXPECT_EQ(q_indices[std::distance(joint_names.begin(), std::find(joint_names.begin(), joint_names.end(), "test_robot_shoulder_pan_joint"))], 2);
}

TEST_F(UrdfReaderTests, testFirstJointFixedNonTrivialTransform)
{
    std::string file_path = ros::package::getPath("rdl_urdfreader") + "/tests/urdf/first_joint_fixed_non_trivial_xform.urdf";
    RobotDynamics::ModelPtr model(new RobotDynamics::Model());

    EXPECT_TRUE(RobotDynamics::Urdf::urdfReadFromFile(file_path.c_str(), model, false, false));

    RobotDynamics::Math::VectorNd q = RobotDynamics::Math::VectorNd::Zero(model->q_size);
    RobotDynamics::Math::VectorNd qdot = RobotDynamics::Math::VectorNd::Zero(model->qdot_size);
    RobotDynamics::Math::VectorNd qddot = RobotDynamics::Math::VectorNd::Zero(model->qdot_size);

    ASSERT_EQ(model->q_size, 1);
    ASSERT_EQ(model->qdot_size, 1);

    RobotDynamics::updateKinematics(*model, q, qdot, qddot);
    RobotDynamics::ReferenceFramePtr frame = model->referenceFrameMap["j1_link"];

    EXPECT_TRUE(model->referenceFrameMap["j1_link"]->getInverseTransformToRoot().r.isApprox(RobotDynamics::Math::Vector3d(0.0, 0.16, 0.8377), 1e-4));
}

TEST_F(UrdfReaderTests, testFixedArm)
{
    std::string file_path = ros::package::getPath("rdl_urdfreader") + "/tests/urdf/test_robot_arm.urdf";
    RobotDynamics::ModelPtr model(new RobotDynamics::Model());
    EXPECT_TRUE(RobotDynamics::Urdf::urdfReadFromFile(file_path.c_str(), model, false, false));

    // First body in URDf is a fixed joint body, so it'll get mashed together with the root body
    EXPECT_EQ(model->mBodies[0].mMass, 4.);
    EXPECT_TRUE(model->mBodies[0].mCenterOfMass.isApprox(RobotDynamics::Math::Vector3d(0., 0., 0.), 1e-14));

    EXPECT_EQ(model->Ib_c[0].Ixx, 0.0061063308908);
    EXPECT_EQ(model->Ib_c[0].Iyx, 0.0);
    EXPECT_EQ(model->Ib_c[0].Izx, 0.0);
    EXPECT_EQ(model->Ib_c[0].Iyy, 0.0061063308908);
    EXPECT_EQ(model->Ib_c[0].Izy, 0.0);
    EXPECT_EQ(model->Ib_c[0].Izz, 0.01125);

    unsigned int id = model->GetBodyId("test_robot_shoulder_link");
    EXPECT_EQ(model->mBodies[id].mMass, 7.778);
    EXPECT_TRUE(model->mBodies[id].mCenterOfMass.isApprox(RobotDynamics::Math::Vector3d(0., 0.01, 0.), 1e-14));

    EXPECT_EQ(model->Ib_c[id].Ixx, 0.0314743125769);
    EXPECT_EQ(model->Ib_c[id].Iyx, 0.);
    EXPECT_EQ(model->Ib_c[id].Izx, 0.);
    EXPECT_EQ(model->Ib_c[id].Iyy, 0.0314743125769);
    EXPECT_EQ(model->Ib_c[id].Izy, 0.);
    EXPECT_EQ(model->Ib_c[id].Izz, 0.021875625);

    id = model->GetParentBodyId(model->GetBodyId("gripper_right_finger_link"));
    EXPECT_EQ(model->GetBodyId("gripper_right_knuckle_link"), id);

    EXPECT_EQ(model->mJoints[1].mJointType, RobotDynamics::JointTypePrismatic);
    EXPECT_EQ(model->GetBodyId("test_robot_shoulder_link"), model->lambda[model->GetBodyId("test_robot_upper_arm_link")]);
}

TEST_F(UrdfReaderTests, testNegative1DofJointAxes)
{
    std::string file_path = ros::package::getPath("rdl_urdfreader") + "/tests/urdf/test_robot_arm.urdf";
    std::string file_path_2 = ros::package::getPath("rdl_urdfreader") + "/tests/urdf/test_robot_arm_neg_jnt_axes.urdf";
    RobotDynamics::ModelPtr model(new RobotDynamics::Model()), model_neg_axes(new RobotDynamics::Model());
    EXPECT_TRUE(RobotDynamics::Urdf::urdfReadFromFile(file_path.c_str(), model, false, false));
    EXPECT_TRUE(RobotDynamics::Urdf::urdfReadFromFile(file_path_2.c_str(), model_neg_axes, false, false));

    RobotDynamics::Math::VectorNd q = RobotDynamics::Math::VectorNd::Zero(model->q_size);
    RobotDynamics::Math::VectorNd q_neg = RobotDynamics::Math::VectorNd::Zero(model->q_size);
    RobotDynamics::Math::VectorNd qd = RobotDynamics::Math::VectorNd::Zero(model->qdot_size);
    RobotDynamics::Math::VectorNd qd_neg = RobotDynamics::Math::VectorNd::Zero(model->qdot_size);
    RobotDynamics::Math::VectorNd qdd = RobotDynamics::Math::VectorNd::Zero(model->qdot_size);
    RobotDynamics::Math::VectorNd qdd_neg = RobotDynamics::Math::VectorNd::Zero(model->qdot_size);
    RobotDynamics::Math::VectorNd tau = RobotDynamics::Math::VectorNd::Zero(model->qdot_size);
    RobotDynamics::Math::VectorNd tau_neg = RobotDynamics::Math::VectorNd::Zero(model->qdot_size);

    for (int i = 0; i < q.size(); i++)
    {
        q[i] = 0.4 * M_PI * static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
        qd[i] = 0.5 * M_PI * static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
        tau[i] = 0.5 * M_PI * static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
        qdd[i] = 0.5 * M_PI * static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
    }

    q_neg = -q;
    qd_neg = -qd;
    qdd_neg = -qdd;
    tau_neg = -tau;

    RobotDynamics::updateKinematics(*model, q, qd, qdd);
    RobotDynamics::updateKinematics(*model_neg_axes, q_neg, qd_neg, qdd_neg);

    for (unsigned int i = 0; i < model->mBodies.size(); i++)
    {
        EXPECT_TRUE(checkSpatialVectorsEpsilonClose(model->v[i], model_neg_axes->v[i], 1e-14));
        EXPECT_TRUE(checkSpatialVectorsEpsilonClose(model->a[i], model_neg_axes->a[i], 1e-14));
    }

    RobotDynamics::forwardDynamics(*model, q, qd, tau, qdd);
    RobotDynamics::forwardDynamics(*model_neg_axes, q_neg, qd_neg, tau_neg, qdd_neg);

    EXPECT_TRUE(qdd.isApprox(-qdd_neg, 1e-14));

    RobotDynamics::inverseDynamics(*model, q, qd, qdd, tau);
    RobotDynamics::inverseDynamics(*model_neg_axes, q_neg, qd_neg, qdd_neg, tau_neg);

    EXPECT_TRUE(tau.isApprox(-tau_neg, 1e-14));
}

TEST_F(UrdfReaderTests, testJointBodyMap)
{
    std::string file_path = ros::package::getPath("rdl_urdfreader") + "/tests/urdf/test_robot_arm.urdf";
    std::map<std::string, std::string> jointBodyMap;

    ASSERT_TRUE(RobotDynamics::Urdf::parseJointBodyNameMapFromFile(file_path.c_str(), jointBodyMap));

    EXPECT_STREQ(jointBodyMap["test_robot_elbow_joint"].c_str(), "test_robot_forearm_link");
    EXPECT_TRUE(jointBodyMap.find("test_robot_ee_fixed_joint") == jointBodyMap.end());  // It's fixed, so shouldn't be here
    EXPECT_STREQ(jointBodyMap["test_robot_shoulder_pan_joint"].c_str(), "test_robot_shoulder_link");

    std::string contents = getFileContents(file_path);
    std::vector<unsigned int> q_indices;
    RobotDynamics::Urdf::parseJointAndQIndex(contents, q_indices);
    EXPECT_EQ(q_indices.size(), 12);
    // EXPECT_TRUE(q_indices[0] == 0);
}

TEST_F(UrdfReaderTests, testFloatingBaseRobot)
{
    RobotDynamics::ModelPtr model(new RobotDynamics::Model());
    std::string path_to_urdf = ros::package::getPath("rdl_urdfreader") + "/tests/urdf/floating_base_robot.urdf";
    EXPECT_TRUE(RobotDynamics::Urdf::urdfReadFromFile(path_to_urdf.c_str(), model, true, false));

    EXPECT_NEAR(model->mBodies[2].mMass, 4., 1e-14);
    EXPECT_TRUE(model->mBodies[2].mCenterOfMass.isApprox(RobotDynamics::Math::Vector3d(0.3, 0.2, 0.1), 1e-14));

    EXPECT_NEAR(model->Ib_c[2].Ixx, 0.1, 1e-14);
    EXPECT_NEAR(model->Ib_c[2].Iyx, 0., 1e-14);
    EXPECT_NEAR(model->Ib_c[2].Izx, 0., 1e-14);
    EXPECT_NEAR(model->Ib_c[2].Iyy, 0.2, 1e-14);
    EXPECT_NEAR(model->Ib_c[2].Izy, 0., 1e-14);
    EXPECT_NEAR(model->Ib_c[2].Izz, 0.3, 1e-14);

    EXPECT_EQ(model->mJoints[1].mJointType, RobotDynamics::JointTypeTranslationXYZ);
    EXPECT_EQ(model->mJoints[2].mJointType, RobotDynamics::JointTypeSpherical);
}

TEST_F(UrdfReaderTests, testFloatingBaseRobotDeduceFloatingBase)
{
    RobotDynamics::ModelPtr model(new RobotDynamics::Model());
    std::string path_to_urdf = ros::package::getPath("rdl_urdfreader") + "/tests/urdf/floating_base_robot.urdf";
    EXPECT_TRUE(RobotDynamics::Urdf::urdfReadFromFile(path_to_urdf, model));

    EXPECT_NEAR(model->mBodies[2].mMass, 4., 1e-14);
    EXPECT_TRUE(model->mBodies[2].mCenterOfMass.isApprox(RobotDynamics::Math::Vector3d(0.3, 0.2, 0.1), 1e-14));

    EXPECT_NEAR(model->Ib_c[2].Ixx, 0.1, 1e-14);
    EXPECT_NEAR(model->Ib_c[2].Iyx, 0., 1e-14);
    EXPECT_NEAR(model->Ib_c[2].Izx, 0., 1e-14);
    EXPECT_NEAR(model->Ib_c[2].Iyy, 0.2, 1e-14);
    EXPECT_NEAR(model->Ib_c[2].Izy, 0., 1e-14);
    EXPECT_NEAR(model->Ib_c[2].Izz, 0.3, 1e-14);

    EXPECT_EQ(model->mJoints[1].mJointType, RobotDynamics::JointTypeTranslationXYZ);
    EXPECT_EQ(model->mJoints[2].mJointType, RobotDynamics::JointTypeSpherical);
}

TEST_F(UrdfReaderTests, testRobotWithFloatingJoint)
{
    RobotDynamics::ModelPtr model(new RobotDynamics::Model());
    std::string path_to_urdf = ros::package::getPath("rdl_urdfreader") + "/tests/urdf/floating_joint_robot.urdf";
    EXPECT_TRUE(RobotDynamics::Urdf::urdfReadFromFile(path_to_urdf.c_str(), model, true, false));

    EXPECT_EQ(model->mJoints[1].mJointType, RobotDynamics::JointTypeTranslationXYZ);
    EXPECT_EQ(model->mJoints[2].mJointType, RobotDynamics::JointTypeEulerXYZ);

    std::string contents = getFileContents(path_to_urdf);
    std::vector<unsigned int> q_indices;
    EXPECT_TRUE(RobotDynamics::Urdf::parseJointAndQIndex(contents, q_indices));

    EXPECT_EQ(q_indices.size(), 1);
    EXPECT_EQ(q_indices[0], 6);
}

TEST_F(UrdfReaderTests, testRobotSingleBodyFloatingBase)
{
    RobotDynamics::ModelPtr model(new RobotDynamics::Model());
    std::string path_to_urdf = ros::package::getPath("rdl_urdfreader") + "/tests/urdf/single_body_floating_base.urdf";
    ASSERT_TRUE(RobotDynamics::Urdf::urdfReadFromFile(path_to_urdf.c_str(), model, true, false));

    std::string contents = getFileContents(path_to_urdf);
    std::vector<unsigned int> q_indices;
    EXPECT_TRUE(RobotDynamics::Urdf::parseJointAndQIndex(contents, q_indices));

    EXPECT_EQ(q_indices.size(), 0);
}

TEST_F(UrdfReaderTests, rotated_inertial)
{
    RobotDynamics::ModelPtr model(new RobotDynamics::Model());
    std::string path_to_urdf = ros::package::getPath("rdl_urdfreader") + "/tests/urdf/rotated_inertial.urdf";
    ASSERT_TRUE(RobotDynamics::Urdf::urdfReadFromFile(path_to_urdf.c_str(), model, true, false));

    RobotDynamics::Math::Matrix3d I1 = RobotDynamics::Math::Matrix3dIdentity;
    I1(0, 0) = 0.03;
    I1(1, 1) = 0.03;
    I1(2, 2) = 0.02;
    EXPECT_TRUE(I1.isApprox(model->mBodies[1].mInertia, 1.e-10));

    RobotDynamics::Math::Matrix3d I2 = RobotDynamics::Math::Matrix3dIdentity;
    I2(0, 0) = 0.03;
    I2(1, 1) = 0.02;
    I2(2, 2) = 0.03;
    EXPECT_TRUE(I2.isApprox(model->mBodies[2].mInertia, 1.e-10));

    RobotDynamics::Math::Matrix3d I3 = RobotDynamics::Math::Matrix3dIdentity;
    I3(0, 0) = 0.02;
    I3(1, 1) = 0.03;
    I3(2, 2) = 0.03;
    EXPECT_TRUE(I3.isApprox(model->mBodies[3].mInertia, 1.e-10));
}

TEST_F(UrdfReaderTests, rotated_inertial_floating_base)
{
    RobotDynamics::ModelPtr model(new RobotDynamics::Model());
    std::string path_to_urdf = ros::package::getPath("rdl_urdfreader") + "/tests/urdf/floating_base_rotated_inertial.urdf";
    ASSERT_TRUE(RobotDynamics::Urdf::urdfReadFromFile(path_to_urdf.c_str(), model, true, false));

    RobotDynamics::Math::Matrix3d I1 = RobotDynamics::Math::Matrix3dIdentity;
    I1(0, 0) = 0.03;
    I1(1, 1) = 0.03;
    I1(2, 2) = 0.02;
    EXPECT_TRUE(I1.isApprox(model->mBodies[2].mInertia, 1.e-10));

    RobotDynamics::Math::Matrix3d I2 = RobotDynamics::Math::Matrix3dIdentity;
    I2(0, 0) = 0.03;
    I2(1, 1) = 0.02;
    I2(2, 2) = 0.03;
    EXPECT_TRUE(I2.isApprox(model->mBodies[3].mInertia, 1.e-10));

    RobotDynamics::Math::Matrix3d I3 = RobotDynamics::Math::Matrix3dIdentity;
    I3(0, 0) = 0.02;
    I3(1, 1) = 0.03;
    I3(2, 2) = 0.03;
    EXPECT_TRUE(I3.isApprox(model->mBodies[4].mInertia, 1.e-10));
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    ::testing::FLAGS_gtest_death_test_style = "threadsafe";
    return RUN_ALL_TESTS();
}