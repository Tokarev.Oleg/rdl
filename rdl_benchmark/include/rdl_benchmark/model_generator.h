#ifndef _MODEL_GENERATOR_H
#define _MODEL_GENERATOR_H

#include "rdl_dynamics/Model.h"

namespace RobotDynamics
{
    class Model;
}

void generate_planar_tree(RobotDynamics::ModelPtr model, int depth);

/* _MODEL_GENERATOR_H */
#endif
