#ifndef _HUMAN36MODEL_H
#define _HUMAN36MODEL_H

#include "rdl_dynamics/Model.h"

namespace RobotDynamics
{
    class Model;
}

void generate_human36model(RobotDynamics::ModelPtr model);

/* _HUMAN36MODEL_H */
#endif
