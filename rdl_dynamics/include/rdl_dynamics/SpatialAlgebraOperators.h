/*
 * Original Copyright (c) 2011-2016 Martin Felis <martin.felis@iwr.uni-heidelberg.de>
 *
 *
 * RDL - Robot Dynamics Library
 * Modifications Copyright (c) 2017 Jordan Lack <jlack1987@gmail.com>
 *
 * Licensed under the zlib license. See LICENSE for more details.
 */

/**
 * @file SpatialAlgebraOperators.h
 */

#ifndef __RDL_SPATIALALGEBRAOPERATORS_H__
#define __RDL_SPATIALALGEBRAOPERATORS_H__

#include <iostream>
#include <cmath>
#include <rdl_dynamics/Quaternion.h>
#include "rdl_dynamics/rdl_eigenmath.h"

namespace RobotDynamics
{
namespace Math
{
/**
 * @brief Converte quaternion to intrinsic ZYX euler angles
 * @param q The quaternion to convert to ZYX angles
 * @param yaw_at_pitch_singularity At Y = +- PI/2 is a singularity in which there are multiple solutions. This will be the yaw value in the output and the
 * roll value is dependent on this. To get the most accurate results at singularity, provide this value as close as possible to desired/reality and the resulting
 * roll value will be near the expected
 */
static inline Vector3d toIntrinsicZYXAngles(const Quaternion& q, double yaw_at_pitch_singularity = 0.)
{
    /** q can be non-normalised quaternion */

    double sqw = q.w() * q.w();
    double sqx = q.x() * q.x();
    double sqy = q.y() * q.y();
    double sqz = q.z() * q.z();
    double unit = sqx + sqy + sqz + sqw;  // if normalised is one, otherwise is correction factor
    double test = q.w() * q.y() - q.z() * q.x();
    if (test > 0.499 * unit)
    {  // singularity at north pole
        return Vector3d(yaw_at_pitch_singularity, M_PI_2, 2. * atan2(q.x(), q.y()) + yaw_at_pitch_singularity);
    }
    if (test < -0.499 * unit)
    {  // singularity at south pole
        return Vector3d(yaw_at_pitch_singularity, -M_PI_2, 2. * atan2(q.x(), q.w()) - yaw_at_pitch_singularity);
    }

    return Vector3d(atan2(2. * (q.w() * q.z() + q.x() * q.y()), 1. - 2. * (sqy + sqz)), asin(2. * test / unit),
                    atan2(2. * (q.w() * q.x() + q.y() * q.z()), 1. - 2. * (sqx + sqy)));
}

/**
 * @brief Get quaternion representation of axis and angle
 * @param axis
 * @param angle_rad
 * @return Quaternion
 */
static inline Quaternion toQuaternion(const Vector3d& axis, double angle_rad)
{
    double d = axis.norm();
    double s2 = std::sin(angle_rad * 0.5) / d;

    return Quaternion(axis[0] * s2, axis[1] * s2, axis[2] * s2, std::cos(angle_rad * 0.5));
}

/**
 * @brief Convert axis angle to quaternion
 */
static inline Quaternion toQuaternion(const AxisAngle& axisAngle)
{
    return toQuaternion(axisAngle.axis(), axisAngle.angle());
}

/**
 * @brief Convert YPR angles to quaternion
 */
static inline Quaternion intrinsicZYXAnglesToQuaternion(const Vector3d& zyx_angles)
{
    return toQuaternion(Vector3d(0., 0., 1.), zyx_angles[0]) * toQuaternion(Vector3d(0., 1., 0.), zyx_angles[1]) * toQuaternion(Vector3d(1., 0., 0.), zyx_angles[2]);
}

/**
 * @brief Convert YPR angles to quaternion
 */
static inline Quaternion intrinsicZYXAnglesToQuaternion(double yaw, double pitch, double roll)
{
    return intrinsicZYXAnglesToQuaternion(Vector3d(yaw, pitch, roll));
}

/**
 * @brief Convert PRY angles to quaternion
 */
static inline Quaternion intrinsicYXZAnglesToQuaternion(const Vector3d& yxz_angles)
{
    return toQuaternion(Vector3d(0., 1., 0.), yxz_angles[0]) * toQuaternion(Vector3d(1., 0., 0.), yxz_angles[1]) * toQuaternion(Vector3d(0., 0., 1.), yxz_angles[2]);
}

/**
 * @brief Convert PRY angles to quaternion
 */
static inline Quaternion intrinsicYXZAnglesToQuaternion(double pitch, double roll, double yaw)
{
    return intrinsicYXZAnglesToQuaternion(Vector3d(pitch, roll, yaw));
}

/**
 * @brief Convert RPY angles to quaternion
 */
static inline Quaternion intrinsicXYZAnglesToQuaternion(const Vector3d& xyz_angles)
{
    return toQuaternion(Vector3d(1., 0., 0.), xyz_angles[0]) * toQuaternion(Vector3d(0., 1., 0.), xyz_angles[1]) * toQuaternion(Vector3d(0., 0., 1.), xyz_angles[2]);
}

/**
 * @brief Convert RPY angles to quaternion
 */
static inline Quaternion intrinsicXYZAnglesToQuaternion(double roll, double pitch, double yaw)
{
    return intrinsicXYZAnglesToQuaternion(Vector3d(roll, pitch, yaw));
}

/**
 * @brief Get axis angle representation of a quaternion
 * @param q
 * @return Axis angle
 */
static inline AxisAngle toAxisAngle(Quaternion q)
{
    double n = q.vec().norm();
    if (n < std::numeric_limits<double>::epsilon())
    {
        n = q.vec().stableNorm();
    }

    AxisAngle axisAngle;
    if (n > 0.0)
    {
        axisAngle.angle() = 2.0 * atan2(n, q.w());
        axisAngle.axis() = q.vec() / n;
    }
    else
    {
        axisAngle.angle() = 0.0;
        axisAngle.axis() << 1.0, 0.0, 0.0;
    }

    return axisAngle;
}

/**
 * @brief convert rotation matrix to quaternion
 */
static inline Quaternion toQuaternion(const Matrix3d& mat)
{
    double trace = mat.trace();

    if (trace > 0.)
    {
        double s = 2. * std::sqrt(trace + 1.);
        return Quaternion((mat(1, 2) - mat(2, 1)) / s, (mat(2, 0) - mat(0, 2)) / s, (mat(0, 1) - mat(1, 0)) / s, 0.25 * s);
    }
    else if ((mat(0, 0) > mat(1, 1)) && (mat(0, 0) > mat(2, 2)))
    {
        double s = 2. * std::sqrt(1. + mat(0, 0) - mat(1, 1) - mat(2, 2));
        return Quaternion(-0.25 * s, (-mat(0, 1) - mat(1, 0)) / s, (-mat(0, 2) - mat(2, 0)) / s, (mat(2, 1) - mat(1, 2)) / s);
    }
    else if (mat(1, 1) > mat(2, 2))
    {
        double s = 2. * std::sqrt(1. + mat(1, 1) - mat(0, 0) - mat(2, 2));
        return Quaternion((-mat(0, 1) - mat(1, 0)) / s, -0.25 * s, (-mat(1, 2) - mat(2, 1)) / s, (mat(0, 2) - mat(2, 0)) / s);
    }
    else
    {
        double s = 2. * std::sqrt(1. + mat(2, 2) - mat(0, 0) - mat(1, 1));
        return Quaternion((-mat(0, 2) - mat(2, 0)) / s, (-mat(1, 2) - mat(2, 1)) / s, -0.25 * s, (mat(1, 0) - mat(0, 1)) / s);
    }
}

/**
 * @brief Convert quaternion to rotation matrix
 */
static inline Matrix3d toMatrix(const Quaternion& q)
{
    double x = q.x();
    double y = q.y();
    double z = q.z();
    double w = q.w();

    return Matrix3d(1. - 2. * (y * y + z * z), 2. * x * y + 2. * w * z, 2. * x * z - 2. * w * y, 2. * x * y - 2. * w * z, 1. - 2. * (x * x + z * z),
                    2. * y * z + 2. * w * x, 2. * x * z + 2. * w * y, 2. * y * z - 2. * w * x, 1. - 2. * (x * x + y * y));
}

/**
 * @brief Convert rotation matrix to intrinsic ZYX euler angles
 * @param m Rotation matrix to convert
 * @param yaw_at_pitch_singularity At Y = +- PI/2 is a singularity in which there are multiple solutions. This will be the yaw value in the output and the
 * roll value is dependent on this. To get the most accurate results at singularity, provide this value as close as possible to desired/reality and the resulting
 * roll value will be near the expected
 */
static inline Vector3d toIntrinsicZYXAngles(const Matrix3d& m, double yaw_at_pitch_singularity = 0.)
{
    // // Assuming the angles are in radians.
    if (m(0, 2) < -0.999)
    {  // singularity at north pole
        return Vector3d(yaw_at_pitch_singularity, M_PI_2, yaw_at_pitch_singularity + atan2(m(1, 0), m(1, 1)));
    }
    if (m(0, 2) > 0.999)
    {  // singularity at south pole
        return Vector3d(yaw_at_pitch_singularity, -M_PI_2, -yaw_at_pitch_singularity + atan2(-m(1, 0), m(1, 1)));
    }

    return Vector3d(atan2(m(0, 1), m(0, 0)), asin(-m(0, 2)), atan2(m(1, 2), m(2, 2)));
}

/**
 * @brief Create a skew symmetric matrix, m, from a 3d vector such that, given two vectors \f$v_1\f$ and \f$v_2\f$,
 * a 3rd vector which is the cross product of the first two is given by, \f$v_3=\tilde{v_1}v_2\f$. The \f$\sim\f$
 * operator is referred to in Featherstones RBDA as the 3d vector cross(\f$\times\f$) operator.
 * @param vector
 * @return A skew symmetric matrix
 */
static inline Matrix3d toTildeForm(const Vector3d& vector)
{
    return Matrix3d(0., -vector[2], vector[1], vector[2], 0., -vector[0], -vector[1], vector[0], 0.);
}

static inline Matrix3d toTildeForm(const double x, const double y, const double z)
{
    return Matrix3d(0., -z, y, z, 0., -x, -y, z, 0.);
}

inline std::ostream& operator<<(std::ostream& output, const SpatialTransform& X)
{
    output << "X.E = " << std::endl << X.E << std::endl;
    output << "X.r = " << X.r.transpose();
    return output;
}

/**
 * @brief Get spatial transform from angle and axis
 * @param angle_rad angle magnitude
 * @param axis normalized 3d vector
 * @return Spatial transform
 */
inline SpatialTransform Xrot(double angle_rad, const Vector3d& axis)
{
    double s, c;

    s = sin(angle_rad);
    c = cos(angle_rad);

    return SpatialTransform(Matrix3d(axis[0] * axis[0] * (1.0f - c) + c, axis[1] * axis[0] * (1.0f - c) + axis[2] * s, axis[0] * axis[2] * (1.0f - c) - axis[1] * s,

                                     axis[0] * axis[1] * (1.0f - c) - axis[2] * s, axis[1] * axis[1] * (1.0f - c) + c, axis[1] * axis[2] * (1.0f - c) + axis[0] * s,

                                     axis[0] * axis[2] * (1.0f - c) + axis[1] * s, axis[1] * axis[2] * (1.0f - c) - axis[0] * s, axis[2] * axis[2] * (1.0f - c) + c

                                     ),
                            Vector3d(0., 0., 0.));
}

/**
 * @brief Get transform with zero translation and pure rotation about x axis
 * @param xrot
 * @return Transform with zero translation and x-rotation
 */
inline SpatialTransform Xrotx(const double& xrot)
{
    double s, c;

    s = sin(xrot);
    c = cos(xrot);
    return SpatialTransform(Matrix3d(1., 0., 0., 0., c, s, 0., -s, c), Vector3d(0., 0., 0.));
}

/**
 * @brief Get transform with zero translation and pure rotation about y axis
 * @param yrot
 * @return Transform with zero translation and y-rotation
 */
inline SpatialTransform Xroty(const double& yrot)
{
    double s, c;

    s = sin(yrot);
    c = cos(yrot);
    return SpatialTransform(Matrix3d(c, 0., -s, 0., 1., 0., s, 0., c), Vector3d(0., 0., 0.));
}

/**
 * @brief Get transform with zero translation and pure rotation about z axis
 * @param zrot
 * @return Transform with zero translation and z-rotation
 */
inline SpatialTransform Xrotz(const double& zrot)
{
    double s, c;

    s = sin(zrot);
    c = cos(zrot);
    return SpatialTransform(Matrix3d(c, s, 0., -s, c, 0., 0., 0., 1.), Vector3d(0., 0., 0.));
}

/**
 * @brief Get transform with zero translation and intrinsic euler z/y/x rotation
 * @param yaw
 * @param pitch
 * @param roll
 * @return Transform with zero translation and z/y/x rotation
 */
inline SpatialTransform XeulerZYX(double yaw, double pitch, double roll)
{
    return Xrotx(roll) * Xroty(pitch) * Xrotz(yaw);
}

/**
 * @brief Get transform defined by intrinsic YPR(yaw->pitch->roll) euler angles
 * @param ypr Vector of euler angles where ypr[0] is yaw, ypr[1] is pitch, and ypr[2] is roll
 *
 * @return spatial transform where rotation component is defined by the YPR rotations
 */
inline SpatialTransform XeulerZYX(const Vector3d& ypr)
{
    return XeulerZYX(ypr[0], ypr[1], ypr[2]);
}

/**
 * @brief Get transform with zero translation and intrinsic euler x/y/z rotation
 * @param roll
 * @param pitch
 * @param yaw
 * @return Transform with zero translation and x/y/z rotation
 */
inline SpatialTransform XeulerXYZ(double roll, double pitch, double yaw)
{
    return Xrotz(yaw) * Xroty(pitch) * Xrotx(roll);
}

/**
 * @brief Get transform with zero translation and euler x/y/z rotation
 * @param intrinsic xyz_angles xyz angles where element 0 is roll, 1 is pitch, and 2 is yaw
 * @return Transform with zero translation and x/y/z rotation
 */
inline SpatialTransform XeulerXYZ(const Vector3d& xyz_angles)
{
    return XeulerXYZ(xyz_angles[0], xyz_angles[1], xyz_angles[2]);
}

inline SpatialTransform XrotQuat(double x, double y, double z, double w)
{
    return SpatialTransform(toMatrix(Quaternion(x, y, z, w)));
}

inline SpatialTransform XrotQuat(const Quaternion& orientation)
{
    return SpatialTransform(toMatrix(orientation));
}

/**
 * @brief Get pure translation transform
 * @param r
 * @return Transform with identity rotation and translation \f$ r \f$
 */
inline SpatialTransform Xtrans(const Vector3d& r)
{
    return SpatialTransform(Matrix3d::Identity(3, 3), r);
}

/**
 * @brief Get the spatial motion cross matrix
 * @param v
 * @return \f$ v\times \f$
 */
inline SpatialMatrix crossm(const SpatialVector& v)
{
    return SpatialMatrix(0, -v[2], v[1], 0, 0, 0, v[2], 0, -v[0], 0, 0, 0, -v[1], v[0], 0, 0, 0, 0, 0, -v[5], v[4], 0, -v[2], v[1], v[5], 0, -v[3], v[2], 0, -v[0], -v[4],
                         v[3], 0, -v[1], v[0], 0);
}

/**
 * @brief Spatial motion cross times spatial motion
 * @param v1
 * @param v2
 * @return \f$ v1\times v2 \f$
 */
inline SpatialVector crossm(const SpatialVector& v1, const SpatialVector& v2)
{
    return SpatialVector(-v1[2] * v2[1] + v1[1] * v2[2], v1[2] * v2[0] - v1[0] * v2[2], -v1[1] * v2[0] + v1[0] * v2[1],
                         -v1[5] * v2[1] + v1[4] * v2[2] - v1[2] * v2[4] + v1[1] * v2[5], v1[5] * v2[0] - v1[3] * v2[2] + v1[2] * v2[3] - v1[0] * v2[5],
                         -v1[4] * v2[0] + v1[3] * v2[1] - v1[1] * v2[3] + v1[0] * v2[4]);
}

/**
 * @brief Get the spatial force cross matrix
 * @param v
 * @return \f$ v\times* \f$
 */
inline SpatialMatrix crossf(const SpatialVector& v)
{
    return SpatialMatrix(0, -v[2], v[1], 0, -v[5], v[4], v[2], 0, -v[0], v[5], 0, -v[3], -v[1], v[0], 0, -v[4], v[3], 0, 0, 0, 0, 0, -v[2], v[1], 0, 0, 0, v[2], 0, -v[0],
                         0, 0, 0, -v[1], v[0], 0);
}

/**
 * @brief Spatial motion cross spatial force
 * @param v1 Spatial motion
 * @param v2 Spatial force
 * @return \f$ v1\times* v2 \f$
 */
inline SpatialVector crossf(const SpatialVector& v1, const SpatialVector& v2)
{
    return SpatialVector(-v1[2] * v2[1] + v1[1] * v2[2] - v1[5] * v2[4] + v1[4] * v2[5], v1[2] * v2[0] - v1[0] * v2[2] + v1[5] * v2[3] - v1[3] * v2[5],
                         -v1[1] * v2[0] + v1[0] * v2[1] - v1[4] * v2[3] + v1[3] * v2[4], -v1[2] * v2[4] + v1[1] * v2[5], +v1[2] * v2[3] - v1[0] * v2[5],
                         -v1[1] * v2[3] + v1[0] * v2[4]);
}

/**
 * @brief Get the rotated linear portion of the spatial vector
 * @param v Spatial vector
 * @param X Spatial transform
 * @return Rotated linear portion of the spatial vector argument
 */
inline Vector3d getLinearPartTransformed(const SpatialVector& v, const SpatialTransform& X)
{
    double v_rxw_x = v[3] - X.r[1] * v[2] + X.r[2] * v[1];
    double v_rxw_y = v[4] - X.r[2] * v[0] + X.r[0] * v[2];
    double v_rxw_z = v[5] - X.r[0] * v[1] + X.r[1] * v[0];

    return Vector3d(X.E(0, 0) * v_rxw_x + X.E(0, 1) * v_rxw_y + X.E(0, 2) * v_rxw_z, X.E(1, 0) * v_rxw_x + X.E(1, 1) * v_rxw_y + X.E(1, 2) * v_rxw_z,
                    X.E(2, 0) * v_rxw_x + X.E(2, 1) * v_rxw_y + X.E(2, 2) * v_rxw_z);
}
}  // namespace Math
}  // namespace RobotDynamics

EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(RobotDynamics::Math::SpatialTransform)

/* __RDL_SPATIALALGEBRAOPERATORS_H__*/
#endif  // ifndef __RDL_SPATIALALGEBRAOPERATORS_H__
