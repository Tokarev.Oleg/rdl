/*
 * RDL - Robot Dynamics Library
 * Copyright (c) 2017 Jordan Lack <jlack1987@gmail.com>
 *
 * Licensed under the zlib license. See LICENSE for more details.
 */

#ifndef __RDL_DYNAMICS_FRAME_ORIENTATION_HPP__
#define __RDL_DYNAMICS_FRAME_ORIENTATION_HPP__

/**
 * @file FrameOrientation.hpp
 * @page frame_orientation Frame Orientation
 *
 * The RobotDynamics::Math::FrameOrientation object is a class for storing an orientation w.r.t a reference frame
 */

#include "rdl_dynamics/FrameObject.hpp"
#include "rdl_dynamics/Quaternion.h"

namespace RobotDynamics
{
namespace Math
{
/**
 * @class FrameOrientation
 * @ingroup reference_frame
 * @brief A Frame object that represents an orientation(quaternion) relative to a reference frame
 */
class FrameOrientation : public FrameObject, public Quaternion
{
  public:
    FrameOrientation() : FrameObject(nullptr), Quaternion(0., 0., 0., 1.)
    {
    }

    explicit FrameOrientation(ReferenceFramePtr referenceFrame) : FrameObject(referenceFrame), Quaternion(0., 0., 0., 1.)
    {
    }

    FrameOrientation(ReferenceFramePtr referenceFrame, Quaternion quat) : FrameObject(referenceFrame), Quaternion(quat)
    {
    }

    FrameOrientation(ReferenceFramePtr referenceFrame, double x, double y, double z, double w) : FrameObject(referenceFrame), Quaternion(x, y, z, w)
    {
    }

    /**
     *
     * @param referenceFrame
     * @param rotation
     *
     * @note assumes rotation is a valid, orthogonal rotation matrix
     */
    FrameOrientation(ReferenceFramePtr referenceFrame, const Matrix3d& E) : FrameObject(referenceFrame), Quaternion(E)
    {
    }

    Math::TransformableGeometricObject* getTransformableGeometricObject()
    {
        return this;
    }

    void setIncludingFrame(const Quaternion& q, ReferenceFramePtr referenceFrame)
    {
        this->referenceFrame = referenceFrame;
        set(q);
    }

    FrameOrientation changeFrameAndCopy(ReferenceFramePtr referenceFrame) const
    {
        FrameOrientation ret = *this;
        ret.changeFrame(referenceFrame);
        return ret;
    }
};
}  // namespace Math
}  // namespace RobotDynamics

#endif  //__RDL_DYNAMICS_FRAME_ORIENTATION_HPP__
