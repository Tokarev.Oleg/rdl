//
// Created by jordan on 4/20/17.
//

#include <gtest/gtest.h>
#include <Eigen/Dense>
#include "rdl_dynamics/Quaternion.h"
#include "UnitTestUtils.hpp"

struct QuaternionFixture : public testing::Test
{
    QuaternionFixture()
    {
        f = Eigen::IOFormat(Eigen::FullPrecision);
    }

    Eigen::IOFormat f;
};

TEST_F(QuaternionFixture, Sanitize)
{
    RobotDynamics::Math::Quaternion q1(0.1, 0.2, 0.3, 0.4);
    RobotDynamics::Math::Quaternion q2(-0.1, -0.2, -0.3, -0.4);
    q1.normalize();
    q2.normalize();
    q2.sanitize();

    EXPECT_TRUE(q1.isApprox(q2, 1.e-12));

    RobotDynamics::Math::Quaternion q3(0.1, 0.2, 0.3, 0.4);
    RobotDynamics::Math::Quaternion q4(0.1, 0.2, 0.3, 0.4);

    q3.normalize();
    q4.normalize();
    q4.sanitize();

    EXPECT_TRUE(q3.isApprox(q4, 1.e-12));
}

TEST_F(QuaternionFixture, EigenConversion)
{
    RobotDynamics::Math::Quaternion q;
    Eigen::Quaterniond q_eig(1., 2., 3., 4.);

    EXPECT_EQ(q.x(), 0.);
    EXPECT_EQ(q.y(), 0.);
    EXPECT_EQ(q.z(), 0.);
    EXPECT_EQ(q.w(), 1.);

    q = q_eig;

    EXPECT_EQ(q.x(), 2.);
    EXPECT_EQ(q.y(), 3.);
    EXPECT_EQ(q.z(), 4.);
    EXPECT_EQ(q.w(), 1.);
}

TEST_F(QuaternionFixture, element_accessors)
{
    RobotDynamics::Math::Quaternion q1(0.2, 0.3, 0.4, 0.1);

    q1.x() = 1.1;
    q1.y() = 2.1;
    q1.z() = 3.1;
    q1.w() = 4.1;

    EXPECT_EQ(1.1, q1.x());
    EXPECT_EQ(2.1, q1.y());
    EXPECT_EQ(3.1, q1.z());
    EXPECT_EQ(4.1, q1.w());

    EXPECT_TRUE(unit_test_utils::checkVector3dEpsilonClose(q1.vec(), Vector3d(1.1, 2.1, 3.1), unit_test_utils::TEST_PREC));
    EXPECT_EQ(q1.w(), 4.1);
}

TEST_F(QuaternionFixture, multiplication)
{
    RobotDynamics::Math::Quaternion q1(0.2, 0.3, 0.4, 0.1), q2(0.2, 0.1, 0.5, 0.3);
    q1.normalize();
    q2.normalize();

    RobotDynamics::Math::Quaternion q_exp(0.5554700788944518, 0.2338821384818745, 0.3800584750330459, -0.7016464154456233);

    EXPECT_TRUE(unit_test_utils::checkVector4dEpsilonClose(q1 * q2, q_exp, unit_test_utils::TEST_PREC));

    q1 *= q2;

    EXPECT_TRUE(unit_test_utils::checkVector4dEpsilonClose(q1, q_exp, unit_test_utils::TEST_PREC));
}

TEST_F(QuaternionFixture, slerp)
{
    RobotDynamics::Math::Quaternion q1(0.2, 0.3, 0.4, 0.1), q2(0.2, 0.1, 0.5, 0.3);
    q1.normalize();
    q2.normalize();

    RobotDynamics::Math::Quaternion q_exp(0.3633161731320073, 0.4782736431579663, 0.7599826545340369, 0.2483587031060483);

    EXPECT_TRUE(unit_test_utils::checkVector4dEpsilonClose(q_exp, q1.slerp(0.2, q2), unit_test_utils::TEST_PREC));

    q1 = q1 * -1.;
    q2 = q2 * -1.;

    EXPECT_TRUE(unit_test_utils::checkVector4dEpsilonClose(q_exp * -1., q1.slerp(0.2, q2), unit_test_utils::TEST_PREC));

    q1 = q1 * -1.;
    q2 = q2 * -1.;

    EXPECT_TRUE(unit_test_utils::checkVector4dEpsilonClose(q2, q1.slerp(1., q2), unit_test_utils::TEST_PREC));
}

TEST_F(QuaternionFixture, conjugate)
{
    RobotDynamics::Math::Quaternion q(0.1, 0.2, -0.3, -0.4);
    EXPECT_TRUE(unit_test_utils::checkVector4dEpsilonClose(RobotDynamics::Math::Quaternion(-0.1, -0.2, 0.3, -0.4), q.conjugate(), unit_test_utils::TEST_PREC));
}

TEST_F(QuaternionFixture, rotate)
{
    RobotDynamics::Math::Quaternion q1(0.1, 0.2, -0.3, -0.4), q2(-0.1, -0.3, -0.2, 0.);
    RobotDynamics::Math::Vector3d v(-0.1, -0.3, -0.2);
    q1.normalize();

    RobotDynamics::Math::Quaternion q_exp = q1.conjugate() * q2 * q1;
    RobotDynamics::Math::Vector3d v_out = q1.rotate(v);

    EXPECT_NEAR(v_out.x(), q_exp.x(), unit_test_utils::TEST_PREC);
    EXPECT_NEAR(v_out.y(), q_exp.y(), unit_test_utils::TEST_PREC);
    EXPECT_NEAR(v_out.z(), q_exp.z(), unit_test_utils::TEST_PREC);
}

TEST_F(QuaternionFixture, swingTwistDecomposition)
{
    double thx = M_PI / 4.;
    double thy = M_PI / 3.;
    double thz = -M_PI / 6.;

    RobotDynamics::Math::Quaternion qx(std::sin(thx / 2.), 0., 0., std::cos(thx / 2.)), qy(0., std::sin(thy / 2.), 0., std::cos(thy / 2.)),
        qz(0., 0., std::sin(thz / 2.), std::cos(thz / 2.));
    RobotDynamics::Math::Quaternion q = qx * qy * qz;
    RobotDynamics::Math::Quaternion q_twist, q_swing;

    q.swingTwistDecomposition(Vector3d(0., 0., 1.), q_swing, q_twist);
    EXPECT_TRUE(q.isApprox(q_swing * q_twist, 1.e-10));

    q.swingTwistDecomposition(Vector3d(0., 1., 0.), q_swing, q_twist);
    EXPECT_TRUE(q.isApprox(q_swing * q_twist, 1.e-10));

    q.swingTwistDecomposition(Vector3d(1., 0., 0.), q_swing, q_twist);
    EXPECT_TRUE(q.isApprox(q_swing * q_twist, 1.e-10));

    q = qz * qy * qx;

    q.swingTwistDecomposition(Vector3d(0., 0., 1.), q_swing, q_twist);
    EXPECT_TRUE(q.isApprox(q_swing * q_twist, 1.e-10));

    q.swingTwistDecomposition(Vector3d(0., 1., 0.), q_swing, q_twist);
    EXPECT_TRUE(q.isApprox(q_swing * q_twist, 1.e-10));

    q.swingTwistDecomposition(Vector3d(1., 0., 0.), q_swing, q_twist);
    EXPECT_TRUE(q.isApprox(q_swing * q_twist, 1.e-10));
}

TEST_F(QuaternionFixture, swingTwistDecompositionEdgeCases)
{
    RobotDynamics::Math::Quaternion q(0., 0., 0., 1);
    RobotDynamics::Math::Quaternion q_twist, q_swing;

    q.swingTwistDecomposition(Vector3d(0., 0., 1.), q_swing, q_twist);

    EXPECT_TRUE(q.isApprox(q_swing * q_twist, 1.e-10));

    q.set(0., 0., 0., -1);
    q.swingTwistDecomposition(Vector3d(0., 1., 0.), q_swing, q_twist);

    EXPECT_TRUE(q.isApprox(q_swing * q_twist, 1.e-10));

    q.set(0., 0., 0., -1);
    q.swingTwistDecomposition(Vector3d(1., 0., 0.), q_swing, q_twist);

    EXPECT_TRUE(q.isApprox(q_swing * q_twist, 1.e-10));
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
