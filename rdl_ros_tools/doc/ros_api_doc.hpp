/** \page rdl_ros_api Ros Api
 *
 * \section functionality Provided Functionality
 *
 * The ROS API provided by the `rdl_ros_tools` package provides a ROS service interface for performing a number
 * of kinematic operations such as coordinate transforms for twists, wrenches, 3D vectors, and points. There are a
 * number of other services provided for computing other quantities that are often useful in robotics. Below each
 * service will be described in detail. In order to not duplicate documentation, this page will not go into detail
 * on the definition of a twist, etc. For this information please look at the documentation for the rdl_dynamics package
 * or refer to the book Rigid Body Dynamics Algorithms by Roy Featherstone.
 *
 * \subsection KinematicsServices Kinematics Service API
 * The <code>rdl_kinematics_interface</code> node is a ROS node that provides ROS services for doing common kinematics operations such as frame changes,
 * computing twsts, etc.
 *
 * \subsubsection Prereq Prerequisits
 * The following are necessary for up-to-date, kinematically correct results from the rdl_kinematics_interface node,
 * - message(s) containing the full state of the robot are being published. Note that the order of the elements in the robot state message is critical. The order must
 * match the RDL model joint order
 * - A valid URDF for your robot is required and will be consumed by the rdl_kinematics_interface node via rosparam
 *
 * By default, the node will look at <code>rdl_kinematics_interface/robot_description</code> for an urdf string from which it will
 * build a RobotDynamics::Model object for doing kinematic operations. The <code>rdl_kinematics_interface</code> node subscribes to the
 * following topics for updating the state of the robot,
 * - State of the joints: <code>rdl_kinematics_interface/joint_states</code>
 * - Joint and Floating Base State: Full state including joint states and floating base odometry(floating base robots only):
 * <code>rdl_kinematics_interface/robot_state</code>
 *
 * The following list details the services provided by this node and a brief description of the provided functionality,
 * - <code>change_frame_point</code>: Change the reference frame a point is expressed in
 * - <code>change_frame_point_array</code>: Change the reference frame an array of points are expressed in
 * - <code>change_frame_3dvector</code>: Change the reference frame a 3d vector is expressed in
 * - <code>change_frame_3dvector_array</code>: Change the reference frame an array of 3d vectors are expressed in
 * - <code>change_frame_wrench</code>: Change the reference frame a wrench is expressed in
 * - <code>change_frame_wrench_array</code>: Change the reference frame an array of wrenches is expressed in
 * - <code>change_frame_twist</code>: Change the reference frame a twist is expressed in
 * - <code>change_frame_twist_array</code>: Change the reference frame an array of twists are expressed in
 * - <code>get_twist</code>: Compute a twist
 * - <code>get_transform</code>: Compute a spatial transform
 * - <code>get_robot_com</code>: Get a robots center of mass position and velocity
 * - <code>get_body_grav_wrench</code>: Get the gravity wrench for a given body
 *
 * If no remapping is desired and you have the appropriate rosparam parameters set, you may run this node by running the following command in the console,
 * @code{.bash}
 * rosrun rdl_ros_tools rdl_kinematics_interface
 * @endcode
 */